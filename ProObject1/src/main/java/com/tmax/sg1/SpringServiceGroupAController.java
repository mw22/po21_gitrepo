package com.tmax.sg1;

import com.tmax.ff.serviceName1;
import com.tmax.dto.Dtoaaa1;
import com.tmax.dto.Dtoaaa1MsgJson;
import com.tmax.test2.serviceName2;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.google.common.base.Throwables;
import org.springframework.stereotype.Controller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.http.HttpStatus;
import jeus.proobject.model.network.context.RequestContext;
import jeus.proobject.core2.service.ServiceName;
import com.tmax.proobject.service.util.SpringControllerUtil;
import com.tmax.proobject.service.util.ProObjectHttpProtocol;
import com.tmax.proobject.service.context.ServiceContextHolder;

@javax.annotation.Generated(
	value = "com.tmaxsoft.sts4.codegen.controller.SpringControllerGenerator",
	date= "22. 6. 15. ���� 4:50",
	comments = ""
)
@Controller
public class SpringServiceGroupAController
{
    
    @Autowired
    serviceName1 _serviceName1;
    
    @Autowired
    serviceName2 _serviceName2;
    @RequestMapping(value="AAA", method=RequestMethod.GET)
    public void executeserviceName1method1_methodName1 (HttpServletRequest request, HttpServletResponse response) throws Throwable {
        response.setContentType("application/json");
        
        Dtoaaa1MsgJson inputMsgJson = new Dtoaaa1MsgJson();
        ProObjectHttpProtocol protocol;
        try {
        	protocol = SpringControllerUtil.unmarshalRequestBody(request.getInputStream(), inputMsgJson);
        } catch (Throwable e) {
            response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
            String errorMsg = "{\"exception\" : \"Unmarshalling Failed : " + Throwables.getStackTraceAsString(e) + "\"}";
            response.getOutputStream().write(errorMsg.getBytes());
            return;
        }
            
        ServiceName serviceName = new ServiceName("Testapp1.ServiceGroupA.serviceName1method1");
        RequestContext requestContext = SpringControllerUtil.createRequestContext(serviceName, protocol.getHeader());
        ServiceContextHolder.getServiceContext().setRequestContext(requestContext);
            
        Dtoaaa1 svcInput = (Dtoaaa1)protocol.getDto();
        Dtoaaa1 svcOutput = null;
            
        try {
            SpringControllerUtil.handlePreService(serviceName, requestContext, svcInput);
        } catch (Throwable e) {
            response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
            String errorMsg = "{\"exception\" : \"PreProcessing Failed : " + Throwables.getStackTraceAsString(e) + "\"}";
            response.getOutputStream().write(errorMsg.getBytes());
            ServiceContextHolder.removeServiceContext();
            return;
        }
        
        try {
            svcOutput = _serviceName1.methodName1(svcInput);
        } catch (Throwable e) {
            try {
                SpringControllerUtil.handleServiceError(serviceName, requestContext, svcInput, e); // rollback
            } catch (Throwable e1) {
                response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
                String errorMsg = "{\"exception\" : \"ServiceError Handling Failed : " + Throwables.getStackTraceAsString(e1) + "\"}";
                response.getOutputStream().write(errorMsg.getBytes());
                ServiceContextHolder.removeServiceContext();
                return;
            }
            response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
            String errorMsg = "{\"exception\" : \"Service Failed : " + Throwables.getStackTraceAsString(e) + "\"}";
            response.getOutputStream().write(errorMsg.getBytes());
            ServiceContextHolder.removeServiceContext();
            return;
        }
        
        try {
            SpringControllerUtil.handlePostService(serviceName, requestContext, svcInput, svcOutput); // commit
        } catch (Throwable e) {
            response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
            String errorMsg = "{\"exception\" : \"Post-Processing Failed : " + Throwables.getStackTraceAsString(e) + "\"}";
            response.getOutputStream().write(errorMsg.getBytes());
            return;
        } finally {
            ServiceContextHolder.removeServiceContext();
        }
        
        protocol.setDto(svcOutput);
        Dtoaaa1MsgJson outputMsgJson = new Dtoaaa1MsgJson();
        
        try {
            byte[] outputProtocolBytes = SpringControllerUtil.marshalResponseBody(protocol, outputMsgJson);
            response.getOutputStream().write(outputProtocolBytes);
        } catch (Throwable e) {
            response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
            String errorMsg = "{\"exception\" : \"Marshalling Failed : " + Throwables.getStackTraceAsString(e) + "\"}";
            response.getOutputStream().write(errorMsg.getBytes());
            return;
        }
        
        return;
    }
    @RequestMapping(value="BBBB", method=RequestMethod.GET)
    public void executeserviceName1method2_methodName2 (HttpServletRequest request, HttpServletResponse response) throws Throwable {
        response.setContentType("application/json");
        
        Dtoaaa1MsgJson inputMsgJson = new Dtoaaa1MsgJson();
        ProObjectHttpProtocol protocol;
        try {
        	protocol = SpringControllerUtil.unmarshalRequestBody(request.getInputStream(), inputMsgJson);
        } catch (Throwable e) {
            response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
            String errorMsg = "{\"exception\" : \"Unmarshalling Failed : " + Throwables.getStackTraceAsString(e) + "\"}";
            response.getOutputStream().write(errorMsg.getBytes());
            return;
        }
            
        ServiceName serviceName = new ServiceName("Testapp1.ServiceGroupA.serviceName1method2");
        RequestContext requestContext = SpringControllerUtil.createRequestContext(serviceName, protocol.getHeader());
        ServiceContextHolder.getServiceContext().setRequestContext(requestContext);
            
        Dtoaaa1 svcInput = (Dtoaaa1)protocol.getDto();
        Dtoaaa1 svcOutput = null;
            
        try {
            SpringControllerUtil.handlePreService(serviceName, requestContext, svcInput);
        } catch (Throwable e) {
            response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
            String errorMsg = "{\"exception\" : \"PreProcessing Failed : " + Throwables.getStackTraceAsString(e) + "\"}";
            response.getOutputStream().write(errorMsg.getBytes());
            ServiceContextHolder.removeServiceContext();
            return;
        }
        
        try {
            svcOutput = _serviceName1.methodName2(svcInput);
        } catch (Throwable e) {
            try {
                SpringControllerUtil.handleServiceError(serviceName, requestContext, svcInput, e); // rollback
            } catch (Throwable e1) {
                response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
                String errorMsg = "{\"exception\" : \"ServiceError Handling Failed : " + Throwables.getStackTraceAsString(e1) + "\"}";
                response.getOutputStream().write(errorMsg.getBytes());
                ServiceContextHolder.removeServiceContext();
                return;
            }
            response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
            String errorMsg = "{\"exception\" : \"Service Failed : " + Throwables.getStackTraceAsString(e) + "\"}";
            response.getOutputStream().write(errorMsg.getBytes());
            ServiceContextHolder.removeServiceContext();
            return;
        }
        
        try {
            SpringControllerUtil.handlePostService(serviceName, requestContext, svcInput, svcOutput); // commit
        } catch (Throwable e) {
            response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
            String errorMsg = "{\"exception\" : \"Post-Processing Failed : " + Throwables.getStackTraceAsString(e) + "\"}";
            response.getOutputStream().write(errorMsg.getBytes());
            return;
        } finally {
            ServiceContextHolder.removeServiceContext();
        }
        
        protocol.setDto(svcOutput);
        Dtoaaa1MsgJson outputMsgJson = new Dtoaaa1MsgJson();
        
        try {
            byte[] outputProtocolBytes = SpringControllerUtil.marshalResponseBody(protocol, outputMsgJson);
            response.getOutputStream().write(outputProtocolBytes);
        } catch (Throwable e) {
            response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
            String errorMsg = "{\"exception\" : \"Marshalling Failed : " + Throwables.getStackTraceAsString(e) + "\"}";
            response.getOutputStream().write(errorMsg.getBytes());
            return;
        }
        
        return;
    }
    @RequestMapping(value="/dd", method=RequestMethod.GET)
    public void executeServiceName1_methodName1s (HttpServletRequest request, HttpServletResponse response) throws Throwable {
        response.setContentType("application/json");
        
        Dtoaaa1MsgJson inputMsgJson = new Dtoaaa1MsgJson();
        ProObjectHttpProtocol protocol;
        try {
        	protocol = SpringControllerUtil.unmarshalRequestBody(request.getInputStream(), inputMsgJson);
        } catch (Throwable e) {
            response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
            String errorMsg = "{\"exception\" : \"Unmarshalling Failed : " + Throwables.getStackTraceAsString(e) + "\"}";
            response.getOutputStream().write(errorMsg.getBytes());
            return;
        }
            
        ServiceName serviceName = new ServiceName("Testapp1.ServiceGroupA.ServiceName1");
        RequestContext requestContext = SpringControllerUtil.createRequestContext(serviceName, protocol.getHeader());
        ServiceContextHolder.getServiceContext().setRequestContext(requestContext);
            
        Dtoaaa1 svcInput = (Dtoaaa1)protocol.getDto();
        Dtoaaa1 svcOutput = null;
            
        try {
            SpringControllerUtil.handlePreService(serviceName, requestContext, svcInput);
        } catch (Throwable e) {
            response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
            String errorMsg = "{\"exception\" : \"PreProcessing Failed : " + Throwables.getStackTraceAsString(e) + "\"}";
            response.getOutputStream().write(errorMsg.getBytes());
            ServiceContextHolder.removeServiceContext();
            return;
        }
        
        try {
            svcOutput = _serviceName1.methodName1s(svcInput);
        } catch (Throwable e) {
            try {
                SpringControllerUtil.handleServiceError(serviceName, requestContext, svcInput, e); // rollback
            } catch (Throwable e1) {
                response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
                String errorMsg = "{\"exception\" : \"ServiceError Handling Failed : " + Throwables.getStackTraceAsString(e1) + "\"}";
                response.getOutputStream().write(errorMsg.getBytes());
                ServiceContextHolder.removeServiceContext();
                return;
            }
            response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
            String errorMsg = "{\"exception\" : \"Service Failed : " + Throwables.getStackTraceAsString(e) + "\"}";
            response.getOutputStream().write(errorMsg.getBytes());
            ServiceContextHolder.removeServiceContext();
            return;
        }
        
        try {
            SpringControllerUtil.handlePostService(serviceName, requestContext, svcInput, svcOutput); // commit
        } catch (Throwable e) {
            response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
            String errorMsg = "{\"exception\" : \"Post-Processing Failed : " + Throwables.getStackTraceAsString(e) + "\"}";
            response.getOutputStream().write(errorMsg.getBytes());
            return;
        } finally {
            ServiceContextHolder.removeServiceContext();
        }
        
        protocol.setDto(svcOutput);
        Dtoaaa1MsgJson outputMsgJson = new Dtoaaa1MsgJson();
        
        try {
            byte[] outputProtocolBytes = SpringControllerUtil.marshalResponseBody(protocol, outputMsgJson);
            response.getOutputStream().write(outputProtocolBytes);
        } catch (Throwable e) {
            response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
            String errorMsg = "{\"exception\" : \"Marshalling Failed : " + Throwables.getStackTraceAsString(e) + "\"}";
            response.getOutputStream().write(errorMsg.getBytes());
            return;
        }
        
        return;
    }
}

