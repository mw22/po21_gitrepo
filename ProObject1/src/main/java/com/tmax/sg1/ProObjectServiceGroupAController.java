package com.tmax.sg1;

import com.tmax.ff.serviceName1;
import com.tmax.dto.Dtoaaa1;
import com.tmax.dto.Dtoaaa1MsgJson;
import com.tmax.test2.serviceName2;
import org.springframework.stereotype.Component;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import jeus.proobject.model.network.context.RequestContext;
import jeus.proobject.core2.service.ServiceName;
import jeus.proobject.model.context.Header;
import com.tmax.proobject.runtime.ProObjectControllerInput;
import com.tmax.proobject.service.util.ProObjectControllerUtil;
import com.tmax.proobject.service.context.ServiceContextHolder;
import com.tmax.proobject.runtime.msg.HeaderMsgJson;

@javax.annotation.Generated(
	value = "com.tmaxsoft.sts4.codegen.controller.POControllerGenerator",
	date= "22. 6. 15. ���� 4:50",
	comments = ""
)
@Component(value="ProObjectServiceGroupAController")
@EnableAutoConfiguration
public class ProObjectServiceGroupAController{
    
    @Autowired
    serviceName1 _serviceName1;
    
    @Autowired
    serviceName2 _serviceName2;
    public Object executeserviceName1method1(ProObjectControllerInput controllerInput) throws Throwable {
    	ServiceName serviceName = controllerInput.getServiceName();
    	RequestContext requestContext = controllerInput.getRequestContext();
    
    	ServiceContextHolder.getServiceContext().setRequestContext(requestContext);
    
    	boolean fromDispatcher = (controllerInput.getServiceInputObject() == null);
    
    	Dtoaaa1 svcInput;
    	if (fromDispatcher) {
    		HeaderMsgJson headerMsg = new HeaderMsgJson();
    		Header header = headerMsg.unmarshal(controllerInput.getHeaderBytes());
    		requestContext.getRequest().setHeader(header);
    		Dtoaaa1MsgJson svcInputMsgJson= new Dtoaaa1MsgJson();
    		svcInput = svcInputMsgJson.unmarshal(controllerInput.getServiceInputBytes());
    	} else {
    		svcInput = (Dtoaaa1) controllerInput.getServiceInputObject();
    	}
    	ProObjectControllerUtil.handlePreService(serviceName, requestContext, svcInput); // getTransaction
    	Dtoaaa1 svcOutput = null;
    	try {
    		svcOutput = _serviceName1.methodName1(svcInput);
    		ProObjectControllerUtil.handlePostService(serviceName, requestContext, svcInput, svcOutput, fromDispatcher); // commit
    	} catch (Throwable e) {
    		ProObjectControllerUtil.handleServiceError(serviceName, requestContext, svcInput, e, fromDispatcher); // rollback
    		throw e;
    	}
    	ServiceContextHolder.removeServiceContext();
    	if (fromDispatcher) {
    		Dtoaaa1MsgJson svcOutputMsgJson= new Dtoaaa1MsgJson();
    		return svcOutputMsgJson.marshal(svcOutput);
    	} else {
    	return svcOutput;
    	}
    }
    public Object executeserviceName1method2(ProObjectControllerInput controllerInput) throws Throwable {
    	ServiceName serviceName = controllerInput.getServiceName();
    	RequestContext requestContext = controllerInput.getRequestContext();
    
    	ServiceContextHolder.getServiceContext().setRequestContext(requestContext);
    
    	boolean fromDispatcher = (controllerInput.getServiceInputObject() == null);
    
    	Dtoaaa1 svcInput;
    	if (fromDispatcher) {
    		HeaderMsgJson headerMsg = new HeaderMsgJson();
    		Header header = headerMsg.unmarshal(controllerInput.getHeaderBytes());
    		requestContext.getRequest().setHeader(header);
    		Dtoaaa1MsgJson svcInputMsgJson= new Dtoaaa1MsgJson();
    		svcInput = svcInputMsgJson.unmarshal(controllerInput.getServiceInputBytes());
    	} else {
    		svcInput = (Dtoaaa1) controllerInput.getServiceInputObject();
    	}
    	ProObjectControllerUtil.handlePreService(serviceName, requestContext, svcInput); // getTransaction
    	Dtoaaa1 svcOutput = null;
    	try {
    		svcOutput = _serviceName1.methodName2(svcInput);
    		ProObjectControllerUtil.handlePostService(serviceName, requestContext, svcInput, svcOutput, fromDispatcher); // commit
    	} catch (Throwable e) {
    		ProObjectControllerUtil.handleServiceError(serviceName, requestContext, svcInput, e, fromDispatcher); // rollback
    		throw e;
    	}
    	ServiceContextHolder.removeServiceContext();
    	if (fromDispatcher) {
    		Dtoaaa1MsgJson svcOutputMsgJson= new Dtoaaa1MsgJson();
    		return svcOutputMsgJson.marshal(svcOutput);
    	} else {
    	return svcOutput;
    	}
    }
    public Object executeServiceName1(ProObjectControllerInput controllerInput) throws Throwable {
    	ServiceName serviceName = controllerInput.getServiceName();
    	RequestContext requestContext = controllerInput.getRequestContext();
    
    	ServiceContextHolder.getServiceContext().setRequestContext(requestContext);
    
    	boolean fromDispatcher = (controllerInput.getServiceInputObject() == null);
    
    	Dtoaaa1 svcInput;
    	if (fromDispatcher) {
    		HeaderMsgJson headerMsg = new HeaderMsgJson();
    		Header header = headerMsg.unmarshal(controllerInput.getHeaderBytes());
    		requestContext.getRequest().setHeader(header);
    		Dtoaaa1MsgJson svcInputMsgJson= new Dtoaaa1MsgJson();
    		svcInput = svcInputMsgJson.unmarshal(controllerInput.getServiceInputBytes());
    	} else {
    		svcInput = (Dtoaaa1) controllerInput.getServiceInputObject();
    	}
    	ProObjectControllerUtil.handlePreService(serviceName, requestContext, svcInput); // getTransaction
    	Dtoaaa1 svcOutput = null;
    	try {
    		svcOutput = _serviceName1.methodName1s(svcInput);
    		ProObjectControllerUtil.handlePostService(serviceName, requestContext, svcInput, svcOutput, fromDispatcher); // commit
    	} catch (Throwable e) {
    		ProObjectControllerUtil.handleServiceError(serviceName, requestContext, svcInput, e, fromDispatcher); // rollback
    		throw e;
    	}
    	ServiceContextHolder.removeServiceContext();
    	if (fromDispatcher) {
    		Dtoaaa1MsgJson svcOutputMsgJson= new Dtoaaa1MsgJson();
    		return svcOutputMsgJson.marshal(svcOutput);
    	} else {
    	return svcOutput;
    	}
    }
}

